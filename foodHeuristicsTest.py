import os
import sys


def testFoodHeuristic(agentName, layoutName="trickySearch"):
    """Test all food heuristics."""
    print agentName
    os.system(
        "python2 pacman.py -l {0} -p {1} --quietTextGraphics".format(
            layoutName, agentName,
        )
    )
    print "\n"


def readLayout(argv):
    from optparse import OptionParser

    parser = OptionParser("")
    parser.add_option('-l', '--layout', dest='layout')
    options, otherjunk = parser.parse_args(argv)
    layout = options.layout
    return layout


if __name__ == '__main__':
    layout = readLayout(sys.argv[1:])

    testFoodHeuristic("AStarAllFoodV10SearchAgent", layout)
    testFoodHeuristic("AStarAllFoodV11SearchAgent", layout)
    testFoodHeuristic("AStarAllFoodV2SearchAgent", layout)
    testFoodHeuristic("AStarAllFoodV3SearchAgent", layout)
