from searchAgents import (
    AStarAllFoodSearchAgent, foodHeuristicV1_0,
    foodHeuristicV1_1, foodHeuristicV2, foodHeuristicV3
)


class AStarAllFoodV10SearchAgent(AStarAllFoodSearchAgent):
    def __init__(self):
        AStarAllFoodSearchAgent.__init__(self, foodHeuristicV1_0)


class AStarAllFoodV11SearchAgent(AStarAllFoodSearchAgent):
    def __init__(self):
        AStarAllFoodSearchAgent.__init__(self, foodHeuristicV1_1)


class AStarAllFoodV2SearchAgent(AStarAllFoodSearchAgent):
    def __init__(self):
        AStarAllFoodSearchAgent.__init__(self, foodHeuristicV2)


class AStarAllFoodV3SearchAgent(AStarAllFoodSearchAgent):
    def __init__(self):
        AStarAllFoodSearchAgent.__init__(self, foodHeuristicV3)
